package plugin

import (
	goplugin "github.com/hashicorp/go-plugin"
)

func Run(pluginName string, pluginInstance Plugin_Interface) {
	//logger := hclog.New(&hclog.LoggerOptions{
	//Level:      hclog.Trace,
	//Output:     os.Stderr,
	//JSONFormat: true,
	//})

	//pluginOne := &Plugin_Impl{
	////logger: logger,
	//}
	// 希望暴露的插件Map
	pluginMap := make(map[string]goplugin.Plugin)

	// plugin
	//pluginMap[pluginOne.GetName()] = &Plugin_Plugin{
	//Impl: pluginOne,
	//}
	pluginMap[pluginName] = &Plugin_Plugin{
		Impl: pluginInstance,
	}

	// 握手协议配置
	var handshakeConfig = goplugin.HandshakeConfig{
		ProtocolVersion:  1,
		MagicCookieKey:   "bcb-serv",
		MagicCookieValue: "bcbpwd",
	}

	goplugin.Serve(&goplugin.ServeConfig{
		HandshakeConfig: handshakeConfig,
		Plugins:         pluginMap,
	})
}
