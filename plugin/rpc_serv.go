package plugin

import "fmt"

//------------------------------------------------
// 实现插件的代理类
//------------------------------------------------
// RPC 服务端
type Plugin_RpcServer struct {
	// 具体的实现
	Impl Plugin_Interface
}

// -------------------- RPC 服务外部接口 --------------
func (s *Plugin_RpcServer) CmdHandler(arg interface{}, resp *string) error {
	inData, ok := arg.(string)
	if !ok {
		return fmt.Errorf("参数解析异常, %v", arg)
	}
	*resp = s.Impl.CmdHandler(inData)
	return nil
}
