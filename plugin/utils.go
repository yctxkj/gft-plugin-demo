package plugin

import (
	"encoding/json"

	"github.com/gogf/gf/frame/g"
)

type JsonExit struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Resutl  interface{} `json:"result"`
}

func Exit(code int, message string, result interface{}) string {
	res := JsonExit{
		Code:    code,
		Message: message,
		Resutl:  result,
	}

	bytRes, err := json.Marshal(res)
	if err != nil {
		g.Log().Errorf("json marshal fail, %v", err)
	}

	sRes := string(bytRes)
	return sRes
}
