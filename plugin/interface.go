package plugin

//------------------------------------------------
// 定义插件接口
//------------------------------------------------
type Plugin_Interface interface {
	CmdHandler(inData string) string
}
