package plugin

import (
	"net/rpc"

	goplugin "github.com/hashicorp/go-plugin"
)

//------------------------------------------------
// 实现插件的代理类
//------------------------------------------------
type Plugin_Plugin struct {
	Impl Plugin_Interface
}

// ------------------- 插件外部接口 ----------------
func (s *Plugin_Plugin) Server(*goplugin.MuxBroker) (interface{}, error) {
	return &Plugin_RpcServer{Impl: s.Impl}, nil
}

func (s *Plugin_Plugin) Client(b *goplugin.MuxBroker, c *rpc.Client) (interface{}, error) {
	return nil, nil
}
