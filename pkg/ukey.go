package pkg

import (
	zaykpkg "gitee.com/yctxkj/zaykukey/pkg"
	"github.com/gogf/gf/frame/g"
)

func GetLibVersion() (*string, error) {
	libVersion, err := zaykpkg.UKey.GetLibVersion()
	if err != nil {
		g.Log().Errorf("取UKEY库版本失败, %v", err)
		return nil, err
	}
	g.Log().Infof("UKEY库版本: %v", *libVersion)
	return libVersion, nil
}
