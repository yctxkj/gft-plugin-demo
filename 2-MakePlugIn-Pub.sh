#!/bin/bash
# -----------------------------------------------------------------
# FileName: 1-MakePlugIn.sh
# Date: 2021-09-15
# Author: jiftle
# Description: 
# -----------------------------------------------------------------

source ./1-MakePlugIn.sh
bash ./1-MakePlugIn.sh

echo "---> 发布插件"
echo "  |---> 删除历史文件"
rm -f  ../gft-serv/plugin/$pluginName/$pluginName.plugin

echo "  |---> 发布插件"
cp -f $pluginName.plugin ../gft-serv/plugin/$pluginName
tree -lh ../gft-serv/plugin/ 
