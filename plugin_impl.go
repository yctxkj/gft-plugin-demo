package main

//------------------------------------------------
// 插件接口实现类
//------------------------------------------------
import (
	"os"
	"time"

	"gitee.com/yctxkj/gft-plugin-demo/pkg"
	"gitee.com/yctxkj/gft-plugin-demo/plugin"

	"github.com/gogf/gf/frame/g"
	hclog "github.com/hashicorp/go-hclog"
)

type Plugin_Impl struct {
	inited bool
	logger *hclog.Logger
}

const cnt_pluginName string = "bcb-cc-call"

func (s *Plugin_Impl) GetName() string {
	return cnt_pluginName
}

func (s *Plugin_Impl) plugin_init() {
	if s.inited {
		return
	}

	logger := hclog.New(&hclog.LoggerOptions{
		Level:      hclog.Trace,
		Output:     os.Stderr,
		JSONFormat: true,
	})
	s.logger = &logger
}

func (s *Plugin_Impl) CmdHandler(inData string) string {
	s.plugin_init()

	// 调用方法
	startTime := time.Now()
	ver, err := pkg.GetLibVersion()
	if err != nil {
		return plugin.Exit(2001, err.Error(), nil)
	}
	elapsed := time.Since(startTime)
	s.LogInfo("CmdHandler, inData=%v, 耗时=%vms", inData, elapsed.Milliseconds())

	return plugin.Exit(200, "success", *ver)
}

func (s *Plugin_Impl) LogInfo(format string, v ...interface{}) {
	g.Log().Infof(format, v...)
	if s.logger != nil {
		(*s.logger).Info(format, v...)
	}
}

func (s *Plugin_Impl) LogError(format string, v ...interface{}) {
	g.Log().Errorf(format, v)
	if s.logger != nil {
		(*s.logger).Error(format, v)
	}
}

func (s *Plugin_Impl) LogWarn(format string, v ...interface{}) {
	g.Log().Warningf(format, v...)
	if s.logger != nil {
		(*s.logger).Warn(format, v...)
	}
}
