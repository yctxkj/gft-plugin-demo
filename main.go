package main

import (
	"flag"

	"gitee.com/yctxkj/gft-plugin-demo/plugin"
)

func main() {
	//定义命令行参数方式1
	var start_type string

	flag.StringVar(&start_type, "type", "serv", "启动类型，插件方式 plugin, 非插件 serv")

	//解析命令行参数
	flag.Parse()

	if start_type == "plugin" {
		plugin.Run("demo", &Plugin_Impl{})
	} else {
		tmain()
	}
}
