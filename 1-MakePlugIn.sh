#!/bin/bash
# -----------------------------------------------------------------
# FileName: 1-MakePlugIn.sh
# Date: 2021-09-15
# Author: jiftle
# Description: 
# -----------------------------------------------------------------

echo "---> 编译插件"
export CGO_ENABLED=1

pluginName="demo"
go build -o $pluginName.plugin
chmod +x $pluginName.plugin

ls -lh |grep plugin |grep $pluginName


